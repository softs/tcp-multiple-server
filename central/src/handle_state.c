#include "state.h"

void handle_state(state *buffer, state *central_state) {
	
	switch (buffer->code) {
	case TEMP_HUM:
		central_state->temperature = buffer->temperature;
		central_state->humidity = buffer->humidity;
		break;

	case PEOPLE_COUNT:
		central_state->people_amount = buffer->people_amount;
		break;

	case SP_T:
		central_state->SP_T_state = buffer->SP_T_state;
		break;

	case SF_T:
		central_state->SF_T_state = buffer->SF_T_state;
		break;

	case SJ_T01:
		central_state->SJ_T01_state = buffer->SJ_T01_state;
		break;

	case SJ_T02:
		central_state->SJ_T02_state = buffer->SJ_T02_state;
		break;

	case SPo_T:
		central_state->SPo_T_state = buffer->SPo_T_state;
		break;

	default:
		break;
	}
}
