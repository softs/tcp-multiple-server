#include "socket.h"

void close_server() {
    close(server_socket);
}

void *init_server(void *central_state) {

	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	printf("Socket inicializado.\n");

	memset(&serv_addr, '0', sizeof(serv_addr));
		
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	//serv_addr.sin_port = htons(DISTRIBUTED_SERVER_1_PORT);
	serv_addr.sin_port = htons(8081);

	bind(server_socket, (struct sockaddr*)&serv_addr,sizeof(serv_addr));

	if(listen(server_socket, 10) == -1){
		printf("Failed to listen\n");
		return 0;
	}

	int bytes_received;
		
	char buffer[5000];

	void *buffer_tmp = &buffer[0];

	while(1){ 

		client_socket = accept(server_socket, (struct sockaddr*)NULL ,NULL);

		if ((bytes_received = recv(client_socket, buffer_tmp, sizeof(state), 0)) < 0) {
			printf("Erro no recv.\n");
		}

		state *params = (state *) buffer_tmp;

		handle_state(params, (state *) central_state);

		sleep(1);
	} 

	return 0;
}