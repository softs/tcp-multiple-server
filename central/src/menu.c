#include "menu.h"

void show_state(state *params) {

    int run = 0;

    while(!run) {

        clear_screen();

        printf("===================================================\n");
        printf("================ Estado dos sensores ==============\n");
        printf("===================================================\n\n");

        printf("Humidade = %.1f%% \nTemperatura = %.1f ºC\n", params->humidity, params->temperature);
        
        printf("Sensor da porta de entrada Terreo: %d\n", params->SPo_T_state);

        printf("Sensor de presenca Terreo: %d\n", params->SP_T_state);

        printf("Sensor de fumaça do Terreo: %d\n", params->SF_T_state);
        
        printf("Sensor de Janela T01: %d\n", params->SJ_T01_state);
        
        printf("Sensor de Janela T02: %d\n", params->SJ_T02_state);

        printf("Sensor de Porta Entrada: %d\n", params->SPo_T_state);
        
        printf("Sensor de Contagem de Pessoas Entrando no Prédio: %d\n", params->SC_IN_state);

        printf("Sensor de Contagem de Pessoas Saindo do Prédio: %d\n", params->SC_OUT_state);

        printf("Total de pessoas: %d\n", params->people_amount);

        printf("\n\n===================================================\n");
        printf("================ Estado dos atuadores ==============\n");
        printf("===================================================\n\n");

        printf("Lâmpada da Sala T01: %d\n", params->LS_T01_state);

        printf("Lâmpada da Sala T02: %d\n", params->LS_T02_state);
        
        printf("Lâmpadas do Corredor Terreo: %d\n", params->LC_T_state);
        
        printf("Ar-Condicionado Terreo: %d\n", params->AC_T_state);
        
        printf("Aspersores de Água (Incêndio): %d\n", params->ASP_state);

        printf("\n\n===================================================\n");
        printf("================ Estado Alarmes ==============\n");
        printf("===================================================\n\n");

        printf("Alarme de presença: %d\n", params->presence_alarm);

        printf("Alarme de incêndio: %d\n", params->fire_alarm);

        printf("\nDigite 1 para voltar para o menu principal: ");

        scanf("%d", &run);
    }

    intro(params);
}

void *intro(void *args) {

    state *params = (state *) args;

    menu(params);

    return 0;
}

void menu(state *params) {
    
    clear_screen();

    printf("\n\n===================================================\n");
    printf("==== Bem vindo ao Sistema de Automacao Predial ====\n");
    printf("===================================================\n\n");
    printf("1) Visualizar estado dos sensores\n");
    printf("2) Acionar lâmpadas\n");
    printf("3) Acionar ar-condicionado\n");
    printf("4) Acionar alarme de presença\n");
    printf("5) Acionar alarme de incêndio\n");
    printf("\nSelecione uma opção: ");
    
    int option;

    scanf("%d", &option);
    
    switch(option) {
        case 1:
            show_state(params);
            break;
        case 2:
            lights_menu(params);
            break;
        case 3:
            air_freezer_menu(params);
            break;
        case 4:
            presence_alarm(params);
            break;
        case 5:
            fire_alarm(params);
            break;

        default:
            printf("\nOpção inválida. Escolha novamente.\n\n");
            menu(params);
    }
}


void lights_menu(state *params) {

    clear_screen();

    printf("\n\n===================================================\n");
    printf("================ Selecione a lâmpada ==============\n");
    printf("===================================================\n\n");

    printf("1) Lâmpada da Sala T01\n");
    printf("2) Lâmpada da Sala T02\n");
    printf("3) Lâmpadas do Corredor Terreo\n");
    printf("4) Lâmpada da Sala 101\n");
    printf("5) Lâmpada da Sala 102\n");
    printf("6) Lâmpadas do Corredor\n");
    printf("7) Sair\n");
    printf("\nSelecione uma opção: ");
    
    int option;
    int turn_on_off;

    scanf("%d", &option);

    switch (option) {
        case 1:
            
            state_option();
            scanf("%d", &turn_on_off);

            if(!check_input(turn_on_off)) {
                menu(params);
            }

            params->LS_T01_state = turn_on_off;
            //params->code = LS_T01;
            params->code_actuator = LS_T01;
            
            enviar_dado_tcp((void *) params);

            /* Csv */
            save_csv("LS_T01", turn_on_off);

            lights_menu(params);
            
            break;

        case 2:
        
            state_option();
        
            scanf("%d", &turn_on_off);

            if(!check_input(turn_on_off)) {
                menu(params);
            }
        
            params->LS_T02_state = turn_on_off;
            // params->code = LS_T02;
            params->code_actuator = LS_T02;
        
            enviar_dado_tcp((void *) params);

            /* Csv */
            save_csv("LS_T02", turn_on_off);

            lights_menu(params);

            break;
        case 3:  

            state_option();
        
            scanf("%d", &turn_on_off);

            if(!check_input(turn_on_off)) {
                menu(params);
            }
        
            params->LC_T_state = turn_on_off;
            //params->code = LC_T;
            params->code_actuator = LC_T;
        
            enviar_dado_tcp((void *) params);

            /* Csv */
            save_csv("LC_T", turn_on_off);

            lights_menu(params);

            break;

        case 4:
            printf("Em construcao...\n");
            lights_menu(params);
            break;
        case 5:
            printf("Em construcao...\n");
            lights_menu(params);
            break;
        case 6:
            printf("Em construcao...\n");
            lights_menu(params);
            break;
        
        case 7:
            menu(params);
            break;
        
        default:
            printf("\nOpção inválida. Escolha novamente.\n\n");
            lights_menu(params);
    }
}

void clear_screen() {
   //system("clear");
}

void state_option() {

    printf("\n\n===================================================\n");
    printf("=============== Selecione uma opção ===============\n");
    printf("===================================================\n\n");
    printf("Digite 0 para desligar\n");
    printf("Digite 1 para ligar\n");
    printf("Opcao: ");
}

void air_freezer_menu(state *params) {
    clear_screen();

    printf("\n\n===================================================\n");
    printf("============ Selecione o Ar Condicionado ==============\n");
    printf("===================================================\n\n");

    printf("1) Ar-Condicionado Terreo\n");
    printf("2) Ar-Condicionado (1º Andar)\n");
    printf("3) Sair\n");
    printf("\nSelecione uma opção: ");
    
    int option;
    int turn_on_off;

    scanf("%d", &option);

    switch (option) {
        case 1:
            
            state_option();
            scanf("%d", &turn_on_off);

            if(!check_input(turn_on_off)) {
                menu(params);
            }

            params->AC_T_state = turn_on_off;
            //params->code = AC_T;
            params->code_actuator = AC_T;
            
            enviar_dado_tcp((void *) params);

            /* Csv */
            save_csv("AC_T", turn_on_off);

            air_freezer_menu(params);
            
            break;

        case 2:
            state_option();
            scanf("%d", &turn_on_off);

            if(!check_input(turn_on_off)) {
                menu(params);
            }

            // params->AC_T_state = turn_on_off;
            // params->code_actuator = AC_1;
            
            // enviar_dado_tcp((void *) params);

            /* Csv */
            save_csv("AC_1", turn_on_off);

            air_freezer_menu(params);
            break;
        
        case 3:
            menu(params);
            break;
        
        default:
            printf("\nOpção inválida. Escolha novamente.\n\n");
            air_freezer_menu(params);
    }
}

void fire_alarm(state *params) {
    clear_screen();

    printf("\n\n===================================================\n");
    printf("======= Alarme de Incendio - Selecione uma opção ======\n");
    printf("===================================================\n\n");
    printf("Digite 0 para desligar\n");
    printf("Digite 1 para ligar\n");
    printf("Opcao: ");

    int turn_on_off;

    scanf("%d", &turn_on_off);

    if(!check_input(turn_on_off)) {
        menu(params);
    }
    
    params->fire_alarm = turn_on_off;

    /* Csv */
    save_csv("ALARM_FIRE", turn_on_off);

    menu(params);
}

void presence_alarm(state *params) {
    clear_screen();

    printf("\n\n===================================================\n");
    printf("======= Alarme de Presença - Selecione uma opção ======\n");
    printf("===================================================\n\n");
    printf("Digite 0 para desligar\n");
    printf("Digite 1 para ligar\n");
    printf("Opcao: ");

    int turn_on_off;

    scanf("%d", &turn_on_off);

    if(!check_input(turn_on_off)) {
        menu(params);
    }
    
    params->presence_alarm = turn_on_off;

    /* Csv */
    save_csv("ALARM_PRESENCE", turn_on_off);

    menu(params);
}

int check_input(int value) {
    return value == 1 || value == 0;
}