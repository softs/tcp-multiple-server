#include "setagem.h"

int main() {

    signal(SIGINT, close_connections);
    signal(SIGKILL, close_connections);

    params = (state *) malloc(sizeof(state));

    /* Initial Setup */
    init_setup(params);

    pthread_create(&server_thread, NULL, init_server, (void *) &params);
    pthread_create(&interface_thread, NULL, intro, (void *) &params);

    pthread_join(server_thread, NULL);
    pthread_join(interface_thread, NULL);

    return 0;
}
