
#ifndef INTERFACE_H_
#define INTERFACE_H_

#include <stdio.h>
#include <stdlib.h>
#include "state.h"
#include "cliente_tcp.h"
#include "csv.h"

void *intro(void *args);
void show_state(state *params);
void menu(state *params);
void clear_screen();
void state_option();
void lights_menu(state *params);
void air_freezer_menu(state *params);
void fire_alarm(state *params);
void presence_alarm(state *param);
int check_input(int value);

#endif /* INTERFACE_H_ */
