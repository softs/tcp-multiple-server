#ifndef STATE_H_
#define STATE_H_

#include <stdlib.h>
#include "default.h"
#include "cliente_tcp.h"

typedef struct {
    /* Init/Stop system */
    int run;

    /* Humidity and Temperature */
    float temperature;
    float humidity;

    /* People Amount */
    int people_amount;
    int SC_IN_state;
    int SC_OUT_state;

    /* Alarm */
    int presence_alarm;
    int fire_alarm;

    /* Ground Floor Actuators State*/
    int LS_T01_state;
    int LS_T02_state;
    int LC_T_state;
    int AC_T_state;
    int ASP_state;

    /* Ground Floor Actuators State*/
    int SP_T_state;
    int SF_T_state;
    int SJ_T01_state;
    int SJ_T02_state;
    int SPo_T_state;

    // /* First Floor State */
    // int LS_101_state;
    // int LS_102_state;
    // int LC_1_state;
    // int AC_1_state;
    // int SP_1_state;
    // int SF_1_state;
    // int SJ_1_state;
    // int SJ_2_state;


    /* Actions */
    int code;
    int code_actuator;

} state;

void handle_state(state *buffer, state *central_state);
void handle_presence_alarm(state *params);
void handle_fire_alarm(state *params);

#endif /* STATE_H_ */