#ifndef STATE_H_
#define STATE_H_

#include <stdio.h>
#include "default.h"

typedef struct {
    /* Init/Stop system */
    int run;

    /* Humidity and Temperature */
    float temperature;
    float humidity;

    /* People Amount */
    int people_amount;
    int SC_IN_state;
    int SC_OUT_state;

    /* Alarm */
    int presence_alarm;
    int fire_alarm;

    /* Ground Floor Actuators State*/
    int LS_T01_state;
    int LS_T02_state;
    int LC_T_state;
    int AC_T_state;
    int ASP_state;

    /* Ground Floor Actuators State*/
    int SP_T_state;
    int SF_T_state;
    int SJ_T01_state;
    int SJ_T02_state;
    int SPo_T_state;

    /* Actions */
    int code;
    int code_actuator;

} state;

#endif /* STATE_H_ */