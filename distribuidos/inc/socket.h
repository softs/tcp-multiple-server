#ifndef SOCKET_H_
#define SOCKET_H_

#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "default.h"
#include "state.h"
#include "cliente_tcp.h"

int server_socket, client_socket;
struct sockaddr_in serv_addr;

void *init_server(void *args);
void close_server();
void handle_state(state *buffer, state *central_state);


#endif