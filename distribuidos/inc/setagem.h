#ifndef SETAGEM_H_
#define SETAGEM_H_

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include "socket.h"
#include "cliente_tcp.h"
#include "default.h"
#include "state.h"
#include "gpio.h"

pthread_t server_thread, sensors_thread, gpio_thread;
state *params;

void init_setup(state *params);
void close_connections(int signal);

#endif