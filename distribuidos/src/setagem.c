#include "setagem.h"

void init_setup(state *params) {

    /* State Inicialization */
    params->run = 1;
    params->code = -1;
    params->code_actuator = -1;
    
    /* Sensors state */
    params->temperature = -0.0;
    params->humidity = 0.0;
    params->people_amount = -1;
    params->SC_IN_state = -1;
    params->SC_OUT_state = -1;
    params->presence_alarm = 0;
    params->fire_alarm = 0;

    /* Actuators state */
    params->LS_T01_state = -1;
    params->LS_T02_state = -1;
    params->LC_T_state = -1;
    params->AC_T_state = -1;
    params->ASP_state = -1;
    params->SP_T_state = -1;
    params->SF_T_state = -1;
    params->SJ_T01_state = -1;
    params->SJ_T02_state = -1;
    params->SPo_T_state = -1;

    init_GPIO();
}

void close_connections(int signal){
    
    pthread_cancel(server_thread);
    pthread_cancel(sensors_thread);
    pthread_cancel(gpio_thread);

    free(params);
    close_server();

    printf("Conexões fechadas\n");
}