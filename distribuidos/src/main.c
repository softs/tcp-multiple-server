#include "setagem.h"

int main() {

    signal(SIGINT, close_connections);

    params = (state *) malloc(sizeof(state));

    init_setup(params);

    pthread_create(&sensors_thread, NULL, handle_sensors, (void *) &params);
    pthread_create(&server_thread, NULL, init_server, (void *) &params);
    pthread_create(&gpio_thread, NULL, handle_actuators, (void *) &params);

    pthread_join(sensors_thread, NULL);
    pthread_join(server_thread, NULL);
    pthread_join(gpio_thread, NULL);

    return 0;
}

