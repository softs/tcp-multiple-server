#include "gpio.h"

void init_GPIO() {
    wiringPiSetup();

    /* Output */
    pinMode(LS_T01, OUTPUT);
    pinMode(LS_T02, OUTPUT);    
    pinMode(LC_T, OUTPUT);
    pinMode(AC_T, OUTPUT);
    pinMode(ASP, OUTPUT);

    /* Input */
    pinMode(TEMP_HUM, INPUT);
    pinMode(SP_T, INPUT);
    pinMode(SPo_T, INPUT);
    pinMode(SJ_T01, INPUT);
    pinMode(SJ_T02, INPUT);
    pinMode(SF_T, INPUT);
    pinMode(SC_IN, INPUT);
    pinMode(SC_OUT, INPUT);
    
}   

int read_gpio(int pin) {
    return digitalRead(pin);
}

void trigger_gpio(int pin, int signal) {
    digitalWrite(pin, signal);
}

// FUNCTION DEFINITIONS
int read_dht_data(state *params) {
    
    uint8_t dht_pin = PIN_DHT11_1;  // default GPIO 20 (wiringPi 28)

    int data[5] = { 0, 0, 0, 0, 0 };
    float temp_cels = -1;
    float temp_fahr = -1;
    float humidity  = -1;
	uint8_t laststate = HIGH;
	uint8_t counter	= 0;
	uint8_t j = 0;
	uint8_t i;

	data[0] = data[1] = data[2] = data[3] = data[4] = 0;

	/* pull pin down for 18 milliseconds */
	pinMode(dht_pin, OUTPUT);
	digitalWrite(dht_pin, LOW);
	delay(18);

	/* prepare to read the pin */
	pinMode(dht_pin, INPUT);

	/* detect change and read data */
	for ( i = 0; i < MAX_TIMINGS; i++ ) {
		counter = 0;
		while ( digitalRead( dht_pin ) == laststate ) {
			counter++;
			delayMicroseconds( 1 );
			if ( counter == 255 ) {
				break;
			}
		}
		laststate = digitalRead( dht_pin );

		if ( counter == 255 )
			break;

		/* ignore first 3 transitions */
		if ( (i >= 4) && (i % 2 == 0) ) {
			/* shove each bit into the storage bytes */
			data[j / 8] <<= 1;
			if ( counter > 16 )
				data[j / 8] |= 1;
			j++;
		}
	}

	/*
	 * check we read 40 bits (8bit x 5 ) + verify checksum in the last byte
	 * print it out if data is good
	 */
	if ( (j >= 40) && (data[4] == ( (data[0] + data[1] + data[2] + data[3]) & 0xFF) ) ) {
		float h = (float)((data[0] << 8) + data[1]) / 10;
		if ( h > 100 ) {
			h = data[0];	// for DHT11
		}
		float c = (float)(((data[2] & 0x7F) << 8) + data[3]) / 10;
		if ( c > 125 ) {
			c = data[2];	// for DHT11
		}
		if ( data[2] & 0x80 ) {
			c = -c;
		}
        
        /* Update State */
        params->temperature = c;
		params->humidity = h;
        
        temp_cels = c;
		temp_fahr = c * 1.8f + 32;
        humidity = h;

		if (DEBUG) printf( "read_dht_data() Humidity = %.1f %% Temperature = %.1f *C (%.1f *F)\n", humidity, temp_cels, temp_fahr );
		return 0; // OK
	} else {
		if (DEBUG) printf( "read_dht_data() Data not good, skip\n" );
		temp_cels = temp_fahr = humidity = -1;
		return 1; // NOK
	}
}

void *handle_actuators(void *args) {

	state *params = (state *) args;
	pthread_mutex_t actuator_mutex = PTHREAD_MUTEX_INITIALIZER;

	while(1) {

		pthread_mutex_lock(&actuator_mutex);

		switch (params->code_actuator) {
			case LS_T01:
				/* Lâmpada da Sala T01 */
				trigger_gpio(LS_T01,  params->LS_T01_state);
				printf("LS_TO1: %d\n", params->LS_T01_state);
				break;
			case LS_T02:
				/* Lâmpada da Sala T02 */
				trigger_gpio(LS_T02, params->LS_T02_state);
				printf("LS_TO2: %d\n", params->LS_T02_state);
				break;
			case LC_T:
				/* Lâmpadas do Corredor Terreo */
				trigger_gpio(LC_T, params->LC_T_state);
				printf("LC_T: %d\n", params->LC_T_state);
				break;
			case AC_T:
				/* Ar-Condicionado Terreo */
				trigger_gpio(AC_T, params->AC_T_state);
				printf("AC_T: %d\n", params->AC_T_state);
				break;
			case ASP:
				/*Aspersores de Água (Incêndio) */
				trigger_gpio(ASP, params->ASP_state);
				printf("ASP: %d\n", params->ASP_state);
				break;
			
			default:
				break;
		}

		params->code_actuator = -1;

		pthread_mutex_unlock(&actuator_mutex);

		sleep(2);
	}
}

void *handle_sensors(void *args) {

	state *params = (state *) args;
	pthread_mutex_t sensor_mutex = PTHREAD_MUTEX_INITIALIZER;

	while(1) {

		pthread_mutex_lock(&sensor_mutex);

		read_dht_data(params);

		printf("\nHumidity = %.1f%% Temperature = %.1f ºC\n", params->humidity, params->temperature);
		
		params->code = TEMP_HUM;

		enviar_dado_tcp((void *) params);
		
		params->SP_T_state = read_gpio(SP_T);

		printf("\nSensor de presenca Terreo: %d\n", params->SP_T_state);

		params->code = SP_T;

		enviar_dado_tcp((void *) params);

		params->SPo_T_state = read_gpio(SPo_T);
		
		printf("\nSensor da porta de entrada Terreo: %d\n", params->SPo_T_state);

		params->code = SPo_T;

		enviar_dado_tcp((void *) params);

		params->SJ_T01_state = read_gpio(SJ_T01);
		
		printf("\nSensor da janela 01 Terreo: %d\n", params->SJ_T01_state);

		params->code = SJ_T01;

		enviar_dado_tcp((void *) params);

		/* Window Sensor 2 */
		params->SJ_T02_state = read_gpio(SJ_T02);
		
		printf("\nSensor da janela 02 Terreo: %d\n", params->SJ_T02_state);

		params->code = SJ_T02;

		enviar_dado_tcp((void *) params);

		params->SF_T_state = read_gpio(SF_T);
		
		printf("\nSensor de fumaça do Terreo: %d\n", params->SF_T_state);

		params->code = SF_T;

		enviar_dado_tcp((void *) params);

		for(int i = 0; i < 5; i++) {

			printf("\n");
			params->SC_IN_state = read_gpio(SC_IN);
			params->SC_OUT_state = read_gpio(SC_OUT);
			params->people_amount = params->people_amount + params->SC_IN_state - params->SC_OUT_state;
			params->people_amount = params->people_amount < 0 ? 0 : params->people_amount;
			usleep(200000);
		}

		printf("\nTotal de pessoas: %d\n", params->people_amount);

		params->code = PEOPLE_COUNT;

		enviar_dado_tcp((void *) params);

		pthread_mutex_unlock(&sensor_mutex);

		sleep(2);
	}

	return 0;
}