#include "socket.h"

void close_server() {
    close(server_socket);
}

void handle_state(state *buffer, state *central_state) {
	
	switch (buffer->code_actuator) {
		case LS_T01:

			/* Lâmpada da Sala T01 */
			central_state->LS_T01_state = buffer->LS_T01_state;
			central_state->code_actuator = buffer->code_actuator;
			break;
		
		case LS_T02:

			/* Lâmpada da Sala T02 */
			central_state->LS_T02_state = buffer->LS_T02_state;
			central_state->code_actuator = buffer->code_actuator;
			break;

		case LC_T:

			/* Lâmpadas do Corredor Terreo */
			central_state->LC_T_state = buffer->LC_T_state;
			central_state->code_actuator = buffer->code_actuator;
			break;

		case AC_T:
			
			/* Ar-Condicionado Terreo */
			central_state->AC_T_state = buffer->AC_T_state;
			central_state->code_actuator = buffer->code_actuator;
			break;

		case ASP:

			/*Aspersores de Água (Incêndio) */
			central_state->ASP_state = buffer->ASP_state;
			central_state->code_actuator = buffer->code_actuator;
			break;
		
		default:
			break;
	}
}

void *init_server(void *args) {

	state *current_data = (state *) args;

	server_socket = socket(AF_INET, SOCK_STREAM, 0);
	printf("Socket inicializado.\n");

	memset(&serv_addr, '0', sizeof(serv_addr));
		
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY); 
	// serv_addr.sin_port = htons(DISTRIBUTED_SERVER_1_PORT);
	serv_addr.sin_port = htons(8080);

	bind(server_socket, (struct sockaddr*)&serv_addr,sizeof(serv_addr));

	if(listen(server_socket, 10) == -1){
		printf("Failed to listen\n");
		return 0;
	}

	int bytes_received;
		
	char buffer[5000];

	void *buffer_tmp = &buffer[0];

	while(1) {

		client_socket = accept(server_socket, (struct sockaddr*)NULL ,NULL);

		if ((bytes_received = recv(client_socket, buffer_tmp, sizeof(state), 0)) < 0) {
			printf("Erro no recv.\n");
		}

		state *params = (state *) buffer_tmp;

		handle_state(params, current_data);

		sleep(1);
	} 

	return 0;
}

